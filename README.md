# NHN Commerce Middleware class v1.0
- NhnService
- NhnREST
- NhnApp
- NhnTokenRepository
- NhnStoreRepository


## 설치
composer.json에 다음과 같은 항목을 추가하고 composer update

```
"require": {
    "lmfriends/lmfds-nhn" : "~1.0.1"
},
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/php-packages1/lmfds-nhn"
    }
]
```


## NHN Commerce 환경 설정
.env
```
NHN_APP_NAME="NHN_APP_NAME"
NHN_APP_URI="https://lmfriends.app/xxx/nhn"
NHN_APP_VERSION="1.0"
NHN_SECRET_KEY="NHN_SECRET_KEY"
NHN_SECRET_KEY="NHN_SECRET_KEY"
```
