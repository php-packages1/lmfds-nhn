<?php

/**
 * NhnStoreRepository - NHN Commerce 상점 정보 저장소
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsNhn\Repositories;

use Lmfriends\LmfdsFoundation\Model;

class NhnStoreRepository extends Model
{
  public function __construct($env, $tableName = 'nhn_stores')
  {
    parent::__construct($env, $tableName);
  }

  public function save($data)
  {
    $mall_id = $this->getValue($data, 'id');
    $phone = $this->getValue($data, 'mobile');
    $email = $this->getValue($data, 'email');
    $mall = isset($data['mall']) ? $data['mall'] : null;
    $business = isset($data['business']) ? $data['business'] : null;
    if (!$mall || !$business) return ['error' => 'not enough filed.'];

    $mall_no = $this->getValue($mall, 'mallNo');
    $client_id = $this->getValue($mall, 'clientId');
    $company_name = $this->getValue($business, 'companyName');
    $domains = array_pop($mall['domains']);
    $mall_url = $this->getValue($domains, 'domain');
    $payload = json_encode($data, JSON_UNESCAPED_UNICODE);
    $updated_at = date('Y-m-d H:i:s');
    $sql = "INSERT INTO {$this->_tableName} (mall_id, mall_no, client_id, company_name, phone, email, mall_url, payload, updated_at)
      VALUES ('$mall_id', '$mall_no', '$client_id', '$company_name', '$phone', '$email', '$mall_url', '$payload', '$updated_at')
      ON DUPLICATE KEY UPDATE
      client_id='$client_id', company_name='$company_name', phone='$phone', email='$email', mall_url='$mall_url', payload='$payload', updated_at='$updated_at'";

    return $this->queryExecute($sql);
  }

  public function read($mallId)
  {
    $condition = "WHERE mall_id = '$mallId'";
    $sql = "SELECT * FROM {$this->_tableName} $condition";
    $record = $this->queryExecute($sql);
    return isset($record[0]) ? $record[0] : null;
  }
}
