<?php

/**
 * NhnTokenRepository - NHN Commerce 엑세스 토큰 저장소
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsNhn\Repositories;

use Lmfriends\LmfdsFoundation\Model;

class NhnTokenRepository extends Model
{
  public function __construct($env, $tableName = 'nhn_tokens')
  {
    parent::__construct($env, $tableName);
  }

  public function save($data)
  {
    $mall_id = $this->getValue($data, 'mall_id');
    $access_token = $this->getValue($data, 'access_token');
    if (!$mall_id || !$access_token) return ['error' => 'not enough filed.'];

    $expires_at = $this->getValue($data, 'expires_at');
    $refresh_token = $this->getValue($data, 'refresh_token');
    $refresh_token_expires_at = $this->getValue($data, 'refresh_token_expires_at');
    $payload = json_encode($data, JSON_UNESCAPED_UNICODE);
    $updated_at = date('Y-m-d H:i:s');
    $sql = "INSERT INTO {$this->_tableName} (mall_id, access_token, expires_at, refresh_token, refresh_token_expires_at, payload, created_at, updated_at)
      VALUES ('$mall_id', '$access_token', '$expires_at', '$refresh_token', '$refresh_token_expires_at', '$payload', '$updated_at', '$updated_at')
      ON DUPLICATE KEY UPDATE
      access_token='$access_token', expires_at='$expires_at', refresh_token='$refresh_token', refresh_token_expires_at='$refresh_token_expires_at', payload='$payload', updated_at='$updated_at'";

    return $this->queryExecute($sql);
  }

  public function read($mallId)
  {
    $condition = "WHERE mall_id = '$mallId'";
    $sql = "SELECT * FROM {$this->_tableName} $condition";
    return $this->queryExecute($sql);
  }
}
