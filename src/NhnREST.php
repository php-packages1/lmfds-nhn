<?php

/**
 * NhnREST - NHN Commerce REST API
 *
 * Version 1.0.1
 */


namespace Lmfriends\LmfdsNhn;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class NhnREST
{
  private $logger;

  private $accessToken;
  private $systemKey;
  private $version;
  private $baseUrl;
  private $URI;
  private $postData;

  public function __construct($accessToken, $systemKey, $version = '1.0')
  {
    $path = explode('/vendor', __DIR__)[0] . '/logs/nhn-rest.log';
    $this->logger = new Logger('NhnREST');
    $this->logger->pushHandler(new StreamHandler($path));

    $this->accessToken = $accessToken;
    $this->systemKey = $systemKey;
    $this->version = $version;
    $this->baseUrl = "https://server-api.e-ncp.com";
    return $this;
  }

  public function http_parse_headers($raw_headers)
  {
    $headers = array();
    $key = '';

    foreach (explode("\n", $raw_headers) as $i => $h) {
      $h = explode(':', $h, 2);

      if (isset($h[1])) {
        if (!isset($headers[$h[0]]))
          $headers[$h[0]] = trim($h[1]);
        elseif (is_array($headers[$h[0]])) {
          $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
        } else {
          $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
        }

        $key = $h[0];
      } else {
        if (substr($h[0], 0, 1) == "\t")
          $headers[$key] .= "\r\n\t" . trim($h[0]);
        elseif (!$key)
          $headers[0] = trim($h[0]);
        trim($h[0]);
      }
    }

    return $headers;
  }

  public function request($requestType)
  {
    $reqCurl = curl_init();
    $reqOption = [
      CURLOPT_URL => $this->URI,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => true,
      CURLOPT_CUSTOMREQUEST => $requestType,
      CURLOPT_HTTPHEADER => [
        'Authorization: Bearer ' . $this->accessToken,
        'systemkey: ' . $this->systemKey,
        'version: ' . $this->version,
        'Content-Type: application/json'
      ]
    ];
    if ($requestType == 'POST' || $requestType == 'PUT') {
      $reqOption[CURLOPT_POSTFIELDS] = json_encode($this->postData);
    }
    curl_setopt_array($reqCurl, $reqOption);

    $sResponse = curl_exec($reqCurl);
    if (curl_errno($reqCurl)) {
      $this->logger->error('request: ' . curl_error($reqCurl), ['extra' => ['option' => $reqOption]]);
      $body = null;
    } else {
      $header_size = curl_getinfo($reqCurl, CURLINFO_HEADER_SIZE);
      $header = substr($sResponse, 0, $header_size);
      $body = substr($sResponse, $header_size);
      $responseHeaders = $this->http_parse_headers($header);
    }

    curl_close($reqCurl);
    $response = $body ? json_decode($body, true) : false;
    if ($response !== false) {
      if (isset($responseHeaders['X-Trace_ID']))
        $response['X-Trace_ID'] = $responseHeaders['X-Trace_ID'];
      else if (isset($responseHeaders['x-trace_id']))
        $response['X-Trace_ID'] = $responseHeaders['x-trace_id'];

      if (isset($response['error']))
        $this->logger->error('response: ', ['extra' => ['response' => $response]]);
    }
    return $response;
  }

  public function me()
  {
    $this->URI = $this->baseUrl . "/auth/me";
    $this->postData = null;
    return $this;
  }

  public function script($data = null)
  {
    $this->URI = $this->baseUrl . "/external-script";
    $this->postData = $data;
    return $this;
  }

  public function mall()
  {
    $this->URI = $this->baseUrl . "/malls";
    $this->postData = null;
    return $this;
  }

  public function orders($options = null)
  {
    $this->URI = $this->baseUrl . "/orders";
    if (is_array($options)) $this->URI .= '?' . http_build_query($options);

    $this->postData = null;
    return $this;
  }

  public function order($id)
  {
    $this->URI = $this->baseUrl . "/orders/$id";
    $this->postData = null;
    return $this;
  }

  public function cart($memberNos, $options = [])
  {
    $this->URI = $this->baseUrl . "/cart";
    $options = array_merge(['memberNos' => $memberNos], $options);
    $this->URI .= '?' . http_build_query($options);

    $this->postData = null;
    return $this;
  }

  public function wish($memberNos, $options = [])
  {
    $this->URI = $this->baseUrl . "/wish";
    $options = array_merge(['memberNos' => $memberNos], $options);
    $this->URI .= '?' . http_build_query($options);

    $this->postData = null;
    return $this;
  }
}
