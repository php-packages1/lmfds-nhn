<?php

/**
 * NhnService - NHN Commerce 서비스
 *
 * Version 1.0.1
 */


namespace Lmfriends\LmfdsNhn;

use Lmfriends\LmfdsFoundation\Repositories\AuthorityRepository;
use Lmfriends\LmfdsNhn\Repositories\NhnStoreRepository;
use Lmfriends\LmfdsNhn\Repositories\NhnTokenRepository;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class NhnService
{
  private $logger;
  private $dbInfo;

  public function __construct()
  {
    $path = explode('/vendor', __DIR__)[0] . '/logs/nhn-service.log';
    $this->logger = new Logger('NhnService');
    $this->logger->pushHandler(new StreamHandler($path));

    $this->dbInfo = [
      'host' => $_ENV['DB_HOST'],
      'dbname' => $_ENV['DB_DATABASE'],
      'username' => $_ENV['DB_USERNAME'],
      'password' => $_ENV['DB_PASSWORD'],
      'charset' => isset($_ENV['DB_CHARSET']) ? $_ENV['DB_CHARSET'] : 'utf8mb4'
    ];
  }

  public function authentication($authorizeCode)
  {
    $formBody = array(
      'grant_type' => 'authorization_code',
      'code' => $authorizeCode,
      'client_secret' => $_ENV['NHN_SECRET_KEY'],
      'client_id' => $_ENV['NHN_SYSTEM_KEY'],
      'redirect_uri' => $_ENV['NHN_APP_URI'] . '/manage'
    );

    $token = $this->requestWithAuthorization($formBody);
    $accessToken = isset($token['access_token']) ? $token['access_token'] : null;
    if ($accessToken) {
      $info = $this->rest(null, $accessToken)->me()->request('GET');
      $this->saveStore(null, null, $info);

      $token = array_merge(['mall_id' => $info['id']], $token);
      $this->saveToken($token);

      $repository = new AuthorityRepository($this->dbInfo);
      $result = $repository->save('nhn', $info['id']);
      if (isset($result['error']))
        $this->logger->error('NhnStoreRepository save', ['extra' => ['result' => $result]]);

      return $token;
    }
    return null;
  }

  public function rest($mallId, $accessToken = null, $version = '1.0')
  {
    $systemKey = $_ENV['NHN_SYSTEM_KEY'];
    if ($accessToken == null)
      $accessToken = $this->getAccessToken($mallId);
    return new NhnREST($accessToken, $systemKey, $version);
  }

  public function getAccessToken($mallId)
  {
    $token = $this->readToken($mallId);
    if (!isset($token[0])) return "";

    $accessToken = $token[0]['access_token'];
    return $accessToken;
  }

  public function saveStore($mallId, $accessToken = null, $store = null)
  {
    if ($store == null)
      $store = $this->rest($mallId, $accessToken)->me()->request('GET');

    $repository = new NhnStoreRepository($this->dbInfo);
    $result = $repository->save($store);
    if (isset($result['error']))
      $this->logger->error('NhnStoreRepository save', ['extra' => ['result' => $result]]);
  }

  protected function requestWithAuthorization($formBody, $version = '1.0')
  {
    $sEndPointUrl = "https://server-api.e-ncp.com/auth/token/long-lived";

    // 발급 받은 인증 코드를 사용하여 실제로 API를 호출할 수 있는 사용자 토큰(Access Token, Refresh Token) 요청
    $oCurl = curl_init();
    $option = array(
      CURLOPT_URL => $sEndPointUrl,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => json_encode($formBody),
      CURLOPT_HTTPHEADER  => array(
        'version: ' . $version,
        'Content-Type: application/json'
      )
    );
    curl_setopt_array($oCurl, $option);
    $sResponse = curl_exec($oCurl);
    if (curl_errno($oCurl)) {
      $this->logger->error('requestWithAuthorization: ' . curl_error($oCurl), ['extra' => ['option' => $option]]);
      $sResponse = null;
    }

    curl_close($oCurl);
    return $sResponse ? json_decode($sResponse, true) : false;
  }

  protected function saveToken($token)
  {
    if (!$token || !isset($token['access_token'])) return '';

    $repository = new NhnTokenRepository($this->dbInfo);
    $result = $repository->save($token);
    if (isset($result['error']))
      $this->logger->error('NhnTokenRepository save', ['extra' => ['result' => $result]]);

    $accessToken = isset($result['success']) ? $token['access_token'] : '';
    return $accessToken;
  }

  protected function readToken($mallId)
  {
    $repository = new NhnTokenRepository($this->dbInfo);
    $result = $repository->read($mallId);
    if (isset($result['error']))
      $this->logger->error('NhnTokenRepository read', ['extra' => ['result' => $result]]);

    return $result;
  }
}
