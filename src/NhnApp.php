<?php

/**
 * NhnApp - NHN Commerce 앱 인증 (앱 진입)
 *
 * Version 1.0.0
 */


namespace Lmfriends\LmfdsNhn;

class NhnApp
{
  private $authorizationCode;
  private $appManageUrl;

  public function __construct($authorizationCode, $appManageUrl = null)
  {
    $this->authorizationCode = $authorizationCode;
    $this->appManageUrl = $appManageUrl;
  }

  public function run()
  {
    $service = new NhnService();
    $token = $service->authentication($this->authorizationCode);
    if (!$token) return;

    $appManageUrl = $this->appManageUrl == null
      ? $_ENV['NHN_APP_URI'] . '/manage/?mall_id=' . $token['mall_id']
      : $this->appManageUrl;
    header('Location: ' . $appManageUrl);
  }
}
