CREATE TABLE IF NOT EXISTS `nhn_stores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` varchar(64) NOT NULL COMMENT '쇼핑몰 아이디',
  `mall_no` varchar(4) DEFAULT '1' COMMENT '쇼핑몰 쇼핑몰 번호',
  `client_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '상호명',
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '전화번호',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '이메일',
  `mall_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '쇼핑몰 주소',
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '원본 데이터',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_mall_id` (`mall_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
